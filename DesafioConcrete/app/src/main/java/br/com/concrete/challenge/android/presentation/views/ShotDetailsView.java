package br.com.concrete.challenge.android.presentation.views;

import java.util.List;

import br.com.concrete.challenge.android.dto.ShotComment;

/**
 * Created by ubiratansoares on 8/16/15.
 */

public interface ShotDetailsView extends LoadingItemsView {

    void fillDribbblePlayerInfo();

    void shotCommentsLoaded(List<ShotComment> comments);

}

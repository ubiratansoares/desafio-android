package br.com.concrete.challenge.android.presentation.views;

import br.com.concrete.challenge.android.dto.ErrorMessageType;

/**
 * Created by ubiratansoares on 8/16/15.
 */
public interface LoadingItemsView {

    void hideLoadingIndicator();

    void showErrorMessage(ErrorMessageType type);

    void hideBlankstateMessage();

    void showLoadingIndicator();

}

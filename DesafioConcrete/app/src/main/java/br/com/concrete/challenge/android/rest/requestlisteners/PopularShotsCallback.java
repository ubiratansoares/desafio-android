package br.com.concrete.challenge.android.rest.requestlisteners;

import java.util.List;

import br.com.concrete.challenge.android.dto.DribbbleShot;
import br.com.concrete.challenge.android.events.OnPopularShotLoaded;
import br.com.concrete.challenge.android.rest.payloads.ShotsPayload;
import de.greenrobot.event.EventBus;
import retrofit.client.Response;

/**
 * Created by ubiratansoares on 8/16/15.
 */

public class PopularShotsCallback extends ErrorHandledCallback<ShotsPayload> {

    @Override public void success(ShotsPayload payload, Response response) {
        final List<DribbbleShot> shots = payload.getShots();
        final int currentPage = payload.getPage();
        EventBus.getDefault().post(new OnPopularShotLoaded(shots, currentPage));
    }

}

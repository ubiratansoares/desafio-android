package br.com.concrete.challenge.android.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ubiratansoares on 8/15/15.
 */

public class DribbbleShot implements Serializable {

    @SerializedName("image_url") String shotURL;
    @SerializedName("views_count") int views;
    @SerializedName("player") DribbbleUser user;
    String title;
    String description;
    int id;

    public int getId() {
        return id;
    }

    public String getShotURL() {
        return shotURL;
    }

    public String getTitle() {
        return title;
    }

    public int getViews() {
        return views;
    }

    public DribbbleUser getUser() {
        return user;
    }

    public String getDescription() {
        return description;
    }
}

package br.com.concrete.challenge.android.dto;

/**
 * Created by ubiratansoares on 8/16/15.
 */

public enum ErrorMessageType {

    NETWORK_ERROR,
    REQUEST_ERROR

}

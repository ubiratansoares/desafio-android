package br.com.concrete.challenge.android.presentation.presenters;

import br.com.concrete.challenge.android.events.OnNetworkError;
import br.com.concrete.challenge.android.events.OnPopularShotLoaded;
import br.com.concrete.challenge.android.events.OnRequestError;
import br.com.concrete.challenge.android.interactor.DribbleAPIInteractor;
import br.com.concrete.challenge.android.presentation.views.PopularShotsView;
import de.greenrobot.event.EventBus;

import static br.com.concrete.challenge.android.dto.ErrorMessageType.NETWORK_ERROR;
import static br.com.concrete.challenge.android.dto.ErrorMessageType.REQUEST_ERROR;

/**
 * Created by ubiratansoares on 8/15/15.
 */

public class PopularShotsPresenter {

    private PopularShotsView view;
    private DribbleAPIInteractor interactor;

    public PopularShotsPresenter(PopularShotsView view) {
        this.view = view;
        interactor = new DribbleAPIInteractor();
    }

    public void resume() {
        EventBus.getDefault().register(this);
    }

    public void pause() {
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(OnPopularShotLoaded event) {
        view.dribbbleShotsLoaded(event.getPopularShots());
        view.hideLoadingIndicator();
        view.hideLoadingIndicator();
    }

    public void onEvent(OnRequestError event) {
        view.showErrorMessage(REQUEST_ERROR);
        view.hideLoadingIndicator();
    }

    public void onEvent(OnNetworkError event) {
        view.showErrorMessage(NETWORK_ERROR);
        view.hideLoadingIndicator();
    }

    public void requireMoreShots(int page) {
        interactor.fetchPopularShots(page);
        view.hideBlankstateMessage();
        view.showLoadingIndicator();
    }

}

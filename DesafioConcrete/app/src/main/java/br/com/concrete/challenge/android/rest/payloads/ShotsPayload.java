package br.com.concrete.challenge.android.rest.payloads;

import java.util.List;

import br.com.concrete.challenge.android.dto.DribbbleShot;

/**
 * Created by ubiratansoares on 8/16/15.
 */

public class ShotsPayload {

    int page;
    int perPage;
    int pages;
    int total;
    List<DribbbleShot> shots;

    public int getPage() {
        return page;
    }

    public int getPerPage() {
        return perPage;
    }

    public int getPages() {
        return pages;
    }

    public int getTotal() {
        return total;
    }

    public List<DribbbleShot> getShots() {
        return shots;
    }

}

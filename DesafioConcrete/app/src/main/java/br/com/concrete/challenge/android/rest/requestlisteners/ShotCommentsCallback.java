package br.com.concrete.challenge.android.rest.requestlisteners;

import java.util.List;

import br.com.concrete.challenge.android.dto.ShotComment;
import br.com.concrete.challenge.android.events.OnShotCommentsLoaded;
import br.com.concrete.challenge.android.rest.payloads.CommentsForShotPayload;
import de.greenrobot.event.EventBus;
import retrofit.client.Response;

/**
 * Created by ubiratansoares on 8/16/15.
 */

public class ShotCommentsCallback extends ErrorHandledCallback<CommentsForShotPayload> {

    @Override public void success(CommentsForShotPayload payload, Response response) {
        final List<ShotComment> comments = payload.getComments();
        EventBus.getDefault().post(new OnShotCommentsLoaded(comments));
    }
}

package br.com.concrete.challenge.android.presentation.presenters;

import br.com.concrete.challenge.android.dto.DribbbleShot;
import br.com.concrete.challenge.android.events.OnNetworkError;
import br.com.concrete.challenge.android.events.OnRequestError;
import br.com.concrete.challenge.android.events.OnShotCommentsLoaded;
import br.com.concrete.challenge.android.interactor.DribbleAPIInteractor;
import br.com.concrete.challenge.android.presentation.views.ShotDetailsView;
import de.greenrobot.event.EventBus;

import static br.com.concrete.challenge.android.dto.ErrorMessageType.NETWORK_ERROR;
import static br.com.concrete.challenge.android.dto.ErrorMessageType.REQUEST_ERROR;

/**
 * Created by ubiratansoares on 8/16/15.
 */

public class ShotDetailsPresenter {

    private DribbleAPIInteractor interactor;
    private ShotDetailsView view;

    public ShotDetailsPresenter(ShotDetailsView view) {
        this.view = view;
        interactor = new DribbleAPIInteractor();
    }

    public void resume() {
        EventBus.getDefault().register(this);
        view.fillDribbblePlayerInfo();

    }

    public void pause() {
        EventBus.getDefault().unregister(this);
    }

    public void fetchCommentsForShot(DribbbleShot shot) {
        interactor.fetchCommentsForShot(shot);
        view.showLoadingIndicator();
        view.hideBlankstateMessage();
    }

    public void onEvent(OnRequestError event) {
        view.showErrorMessage(REQUEST_ERROR);
        view.hideLoadingIndicator();
    }

    public void onEvent(OnNetworkError event) {
        view.showErrorMessage(NETWORK_ERROR);
        view.hideLoadingIndicator();
    }

    public void onEvent(OnShotCommentsLoaded event) {
        view.shotCommentsLoaded(event.getComments());
        view.hideLoadingIndicator();
    }
}

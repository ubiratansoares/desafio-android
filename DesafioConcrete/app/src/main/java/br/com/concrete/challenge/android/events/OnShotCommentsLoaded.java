package br.com.concrete.challenge.android.events;

import java.util.List;

import br.com.concrete.challenge.android.dto.ShotComment;

/**
 * Created by ubiratansoares on 8/16/15.
 */

public class OnShotCommentsLoaded {

    private List<ShotComment> comments;

    public OnShotCommentsLoaded(List<ShotComment> comments) {
        this.comments = comments;
    }

    public List<ShotComment> getComments() {
        return comments;
    }
}

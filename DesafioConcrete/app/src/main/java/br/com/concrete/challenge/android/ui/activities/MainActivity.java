package br.com.concrete.challenge.android.ui.activities;

import android.os.Bundle;

import br.com.concrete.challenge.android.R;
import br.com.concrete.challenge.android.ui.fragments.PopularShotsFragment;

public class MainActivity extends BaseActivity {

    @Override protected int layoutResource() {
        return R.layout.activity_fragment_container;
    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container, new PopularShotsFragment())
                .commit();
    }
}

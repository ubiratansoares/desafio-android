package br.com.concrete.challenge.android.dto;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ubiratansoares on 8/16/15.
 */

public class ShotComment {

    @SerializedName("body") String comment;
    @SerializedName("player") DribbbleUser commenter;

    public String getComment() {
        return comment;
    }

    public DribbbleUser getCommenter() {
        return commenter;
    }
}

package br.com.concrete.challenge.android.presentation.views;

import java.util.List;

import br.com.concrete.challenge.android.dto.DribbbleShot;

/**
 * Created by ubiratansoares on 8/15/15.
 */

public interface PopularShotsView extends LoadingItemsView {

    void dribbbleShotsLoaded(List<DribbbleShot> shots);

}

package br.com.concrete.challenge.android.rest.payloads;

import java.util.List;

import br.com.concrete.challenge.android.dto.ShotComment;

/**
 * Created by ubiratansoares on 8/16/15.
 */

public class CommentsForShotPayload {

    List<ShotComment> comments;

    public List<ShotComment> getComments() {
        return comments;
    }
}

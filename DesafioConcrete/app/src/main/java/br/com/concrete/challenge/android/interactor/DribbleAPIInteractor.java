package br.com.concrete.challenge.android.interactor;

import br.com.concrete.challenge.android.dto.DribbbleShot;
import br.com.concrete.challenge.android.rest.DribbbleAPI;
import br.com.concrete.challenge.android.rest.RestAdapterProvider;
import br.com.concrete.challenge.android.rest.requestlisteners.PopularShotsCallback;
import br.com.concrete.challenge.android.rest.requestlisteners.ShotCommentsCallback;
import retrofit.RestAdapter;

/**
 * Created by ubiratansoares on 8/16/15.
 */

public class DribbleAPIInteractor {

    private DribbbleAPI api;

    public DribbleAPIInteractor() {
        RestAdapter adapter = RestAdapterProvider.instance().getAdapter();
        api = adapter.create(DribbbleAPI.class);
    }

    public void fetchPopularShots(int page) {
        api.popularShots(page, new PopularShotsCallback());
    }

    public void fetchCommentsForShot(DribbbleShot shot) {
        api.commentsForShot(shot.getId(), new ShotCommentsCallback());
    }
}

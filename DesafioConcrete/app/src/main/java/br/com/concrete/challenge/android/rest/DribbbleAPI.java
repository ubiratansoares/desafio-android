package br.com.concrete.challenge.android.rest;

import br.com.concrete.challenge.android.rest.payloads.CommentsForShotPayload;
import br.com.concrete.challenge.android.rest.payloads.ShotsPayload;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by ubiratansoares on 8/15/15.
 */

public interface DribbbleAPI {

    @GET("/shots/popular") void popularShots(
            @Query("page") int page,
            Callback<ShotsPayload> callback
    );

    @GET("/shots/{shotId}/comments/") void commentsForShot(
            @Path("shotId") int shotId,
            Callback<CommentsForShotPayload> callback
    );

}
